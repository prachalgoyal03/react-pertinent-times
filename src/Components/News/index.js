import React from 'react';

import axios from 'axios';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default class PersonList extends React.Component {
  state = {
    persons: [],
    
  }
  renderCard = (card, index) => {
    return (
      <Card style={{ width: "100%", margin: "10px" }} key={index} className="box">
        <Card.Img variant="top" style={{ width: "30rem", hieght: "5rem" }} src= {card.multimedia[0].url} />

      <Card.Body>
      <Card.Title>{card.title}</Card.Title>
          <Card.Text>{card.text}</Card.Text>
        <Button variant="danger" href={card.url}>Click to know more</Button>
        
      </Card.Body>
      </Card>
      
    );
  };

  componentDidMount() {
    axios.get(`https://api.nytimes.com/svc/topstories/v2/home.json?api-key=emFi7AkaLflNp1203JeNRkhsQAUZT2wQ`)

      .then(res => {
        const persons = res.data.results;
        this.setState({ persons });
        //{ this.state.persons.map(person => <li>{person.title}</li>)}
      })
      
  }

  render() {
    return (  
        
        <div className="grid">{this.state.persons.map(this.renderCard)}</div>
            
        )}
    
  
}