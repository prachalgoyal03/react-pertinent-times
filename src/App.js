// import logo from './logo.svg';
import PersonList  from "./Components/News";
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>The Pertinent times</h1>
      <header className="App-header">
        <PersonList></PersonList>
      </header>
    </div>
  );
}

export default App;
